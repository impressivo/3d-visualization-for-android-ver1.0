package co.futureplay.multitouch;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chope on 2014. 8. 20..
 */
public class VisualizationModel {
    public FPColor gridColor;
    public FPColor clearColor;
    public FPColor valueColor;
    private int gridWidth;
    private int gridHeight;

    public List<FPPressurePoint> points;

    public VisualizationModel(int width, int height) {
        this.clearColor = FPColor.blackColor();
        this.gridColor = FPColor.whiteColor();
        this.valueColor = FPColor.redColor();

        this.gridWidth = width;
        this.gridHeight = height;

        this.points = new ArrayList<FPPressurePoint>();
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void clear() {
        this.points.clear();
    }

    public void addPoint(FPPressurePoint point) {
        this.points.add(point);
    }

    public int countOfPoints() {
        return this.points.size();
    }

    public FPPressurePoint getPoint(int index) {
        try {
            return this.points.get(index);
        } catch (IndexOutOfBoundsException e) {
            Log.d("FuturePlay", "index : " + index);
        }
        return null;
    }
}
