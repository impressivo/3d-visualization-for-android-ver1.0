package co.futureplay.multitouch;

/**
 * Created by chope on 2014. 8. 19..
 */
public class FPPressurePoint {
    public float x;
    public float y;
    public float pressure;

    public FPPressurePoint(float x, float y, float pressure) {
        this.x = x;
        this.y = y;
        this.pressure = pressure;
    }

    public String toString() {
        return String.format("x:%.1f y:%.1f p:%.1f", x, y, pressure);
    }
}
