package co.futureplay.multitouch;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class FuturePlayGLSurfaceView extends GLSurfaceView {
    private static final String TAG = "FuturePlay";
    private Renderer renderer;

    /** touch */
    private ScaleGestureDetector scaleDetector;
    private float lastTouchX;
    private float lastTouchY;

    public FuturePlayGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        scaleDetector = new ScaleGestureDetector(context, new ZoomGestureListener());
    }

    @Override
    public void setRenderer(GLSurfaceView.Renderer renderer) {
        super.setRenderer(renderer);
        this.renderer = (Renderer) renderer;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) {
            scaleDetector.onTouchEvent(event);
            return true;
        }

        if (lastTouchX == 0 && lastTouchY == 0) {
            lastTouchX = event.getX();
            lastTouchY = event.getY();
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float diffX = event.getX() - lastTouchX;
            float diffY = event.getY() - lastTouchY;

            renderer.rotateHorizontal -= diffX;
            renderer.rotateVertical += diffY;

            requestRender();

            lastTouchX = event.getX();
            lastTouchY = event.getY();
        }
        else if (event.getAction() == MotionEvent.ACTION_UP) {
            lastTouchX = 0;
            lastTouchY = 0;
        }

        return true;
    }

    public Renderer getRenderer() {
        return renderer;
    }

    private class ZoomGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            renderer.zoom *= detector.getScaleFactor();
            renderer.zoom = Math.max(0.1f, Math.min(renderer.zoom, 5.0f));

            invalidate();
            requestRender();

            return true;
        }
    }

    public static class Renderer implements GLSurfaceView.Renderer {
        public FPColor clearColor;
        public float zoom = 1.0f;
        public float rotateHorizontal = 0f;
        public float rotateVertical = 0f;

        /** FPS */
        private long lastTime = System.nanoTime();
        private long timeElapsed;
        private int countOfFrames;

        public Renderer() {
            clearColor = FPColor.blackColor();
        }

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig eglConfig) {
            gl.glShadeModel(GL10.GL_SMOOTH);
            gl.glClearColor(clearColor.red, clearColor.green, clearColor.blue, clearColor.alpha);
            gl.glClearDepthf(1.0f);
            gl.glEnable(GL10.GL_DEPTH_TEST);
            gl.glDepthFunc(GL10.GL_LEQUAL);
            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            gl.glViewport(0, 0, width, height);
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();

            GLU.gluPerspective(gl, 45.0f, (float) width / height, 1.0f, 30.0f);
            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadIdentity();
        }

        @Override
        public void onDrawFrame(GL10 gl) {
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            gl.glLoadIdentity();

            float[] rotateMatrix = new float[16];
            float[] resultVector = new float[4];
            float[] cameraVector = {
                    -3.0f / zoom,
                    0,
                    3.0f / zoom,
                    1.0f
            };

            Matrix.setIdentityM(rotateMatrix, 0);
            Matrix.rotateM(rotateMatrix, 0, rotateHorizontal, 0, 0, 1.0f);
            Matrix.rotateM(rotateMatrix, 0, rotateVertical, 0, 1.0f, 0);
            Matrix.multiplyMV(resultVector, 0, rotateMatrix, 0, cameraVector, 0);
            GLU.gluLookAt(gl, resultVector[0], resultVector[1], resultVector[2], 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
        }

        protected void computeFPS() {
            long currentTime = System.nanoTime();
            timeElapsed += (currentTime - lastTime);
            countOfFrames++;

            if (timeElapsed > 1000000000) {
                int sec = (int) (timeElapsed / 1000000000.0);

                if (sec > 0) {
                    float fps = countOfFrames / sec;
                    displayFPS(fps);

                    timeElapsed = 0;
                    countOfFrames = 0;
                }
            }

            lastTime = System.nanoTime();
        }

        protected void displayFPS(float fps) {
            Log.d(TAG, "fps : " + fps);
        }

        public FloatBuffer getFloatBufferFromArray(float[] values) {
            ByteBuffer vbb = ByteBuffer.allocateDirect(values.length * 4);
            vbb.order(ByteOrder.nativeOrder());

            FloatBuffer vertexBuffer = vbb.asFloatBuffer();
            vertexBuffer.put(values);
            vertexBuffer.position(0);

            return vertexBuffer;
        }

        public ShortBuffer getShortBufferFromArray(short[] values) {
            ByteBuffer vbb = ByteBuffer.allocateDirect(values.length * 4);
            vbb.order(ByteOrder.nativeOrder());

            ShortBuffer vertexBuffer = vbb.asShortBuffer();
            vertexBuffer.put(values);
            vertexBuffer.position(0);

            return vertexBuffer;
        }

        public void drawLine(GL10 gl, FPColor color, float[] vertices) {
            gl.glColor4f(color.red, color.green, color.blue, color.alpha);

            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, getFloatBufferFromArray(vertices));
            gl.glDrawArrays(GL10.GL_LINES, 0, vertices.length / 3);
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }

        public void drawCoordinates(GL10 gl, FPColor xAxisColor, FPColor yAxisColor, FPColor zAxisColor) {
            float start = -100;
            float end = 100;

            drawLine(gl, xAxisColor, new float[]{ start, 0.0f, 0.0f, end, 0.0f, 0.0f });
            drawLine(gl, yAxisColor, new float[]{ 0.0f, start, 0.0f, 0.0f, end, 0.0f });
            drawLine(gl, zAxisColor, new float[]{ 0.0f, 0.0f, start, 0.0f, 0.0f, end });
        }

        public void drawHexahedron(GL10 gl, float centerX, float centerY, float sizeX, float sizeY, float height, float[] colors) {
            float halfIntervalX = sizeX / 2.0f;
            float halfIntervalY = sizeY / 2.0f;

            float vertices[] = {
                    centerX + halfIntervalX, centerY - halfIntervalY, height,
                    centerX - halfIntervalX, centerY - halfIntervalY, height,
                    centerX - halfIntervalX, centerY - halfIntervalY, 0.0f,
                    centerX + halfIntervalX, centerY - halfIntervalY, 0.0f,
                    centerX + halfIntervalX, centerY + halfIntervalY, 0.0f,
                    centerX + halfIntervalX, centerY + halfIntervalY, height,
                    centerX - halfIntervalX, centerY + halfIntervalY, height,
                    centerX - halfIntervalX, centerY + halfIntervalY, 0.0f,
            };

            short indices[] = {
                    0,1,2,
                    2,3,0,
                    0,3,4,
                    4,5,0,
                    0,5,6,
                    6,1,0,
                    1,6,7,
                    7,2,1,
                    7,4,3,
                    3,2,7,
                    4,7,6,
                    6,5,4
            };

            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, getFloatBufferFromArray(vertices));
            gl.glColorPointer(4, GL10.GL_FLOAT, 0, getFloatBufferFromArray(colors));
            gl.glDrawElements(GL10.GL_TRIANGLES, indices.length, GL10.GL_UNSIGNED_SHORT, getShortBufferFromArray(indices));
            gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }
}
