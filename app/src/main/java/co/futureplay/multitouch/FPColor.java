package co.futureplay.multitouch;

/**
 * Created by chope on 2014. 8. 19..
 */
public class FPColor {
    public float red;
    public float green;
    public float blue;
    public float alpha;

    private FPColor() {
    }

    public static FPColor color(float red, float green, float blue, float alpha) {
        FPColor color = new FPColor();
        color.red = red;
        color.green = green;
        color.blue = blue;
        color.alpha = alpha;
        return color;
    }

    public static FPColor color(float red, float green, float blue) {
        return FPColor.color(red, green, blue, 1.0f);
    }

    public static FPColor blackColor() {
        return FPColor.color(0.0f, 0,0f, 0.0f);
    }

    public static FPColor whiteColor() {
        return FPColor.color(1.0f, 1.0f, 1.0f);
    }

    public static FPColor redColor() {
        return FPColor.color(1.0f, 0.0f, 0.0f);
    }

    public static FPColor greenColor() {
        return FPColor.color(0.0f, 1.0f, 0.0f);
    }

    public static FPColor blueColor() {
        return FPColor.color(0.0f, 0.0f, 1.0f);
    }
}
