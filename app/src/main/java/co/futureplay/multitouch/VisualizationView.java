package co.futureplay.multitouch;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class VisualizationView extends FuturePlayGLSurfaceView {
    public VisualizationView(Context context, AttributeSet attrs) {
        super(context, attrs);

        VisualizationRenderer renderer = new VisualizationRenderer(30, 40);
        setRenderer(renderer);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public VisualizationRenderer getRenderer() {
        return (VisualizationRenderer) super.getRenderer();
    }
}
