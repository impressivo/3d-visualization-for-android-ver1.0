package co.futureplay.multitouch;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class VisualizationRenderer extends FuturePlayGLSurfaceView.Renderer {

    private float marginX;
    private float marginY;

    /** grid */
    public FPColor gridColor;
    private int gridWidth;
    private int gridHeight;
    private float gridInterval;

    public final List<FPPressurePoint> touchPointList = new ArrayList<FPPressurePoint>();

    public VisualizationRenderer(int gridWidth, int gridHeight) {
        float intervalX = 2.0f / (gridWidth - 1);
        float intervalY = 2.0f / (gridHeight- 1);

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.gridInterval = (intervalX < intervalY) ? intervalX : intervalY;
        this.marginX = (2.0f - (gridInterval * (gridWidth - 1))) / 2.0f;
        this.marginY = (2.0f - (gridInterval * (gridHeight - 1))) / 2.0f;
        this.rotateHorizontal = 45;
        this.gridColor = FPColor.whiteColor();
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

//        drawCoordinates(gl, FPColor.redColor(), FPColor.greenColor(), FPColor.blueColor());
        drawGrid(gl);

        for (int i=0; i<touchPointList.size(); i++) {
            FPPressurePoint point = touchPointList.get(i);

            if (point != null) {
                float x = -1.0f + marginX + (gridInterval * point.x);
                float y = -1.0f + marginY + (gridInterval * point.y);
                float height = point.pressure / 255.0f;
                float colors[] = {
                        1.0f, 0.0f, 0.0f, 1.0f,
                        0.9f, 0.0f, 0.0f, 1.0f,
                        0.8f, 0.0f, 0.0f, 1.0f
                };

                drawHexahedron(gl, x, y, gridInterval, gridInterval, height, colors);
            }
        }
    }

    private void drawGrid(GL10 gl) {
        for (float x = (-1.0f + marginX), i = 0; i < gridWidth; x += gridInterval, i++) {
            drawLine(gl, gridColor, new float[]{x, -1.0f + marginY, 0.0f, x, 1.0f - marginY, 0.0f});
        }

        for (float y = (-1.0f + marginY), i = 0; i < gridHeight; y += gridInterval, i++) {
            drawLine(gl, gridColor, new float[]{ -1.0f + marginX, y, 0.0f, 1.0f - marginX, y, 0.0f });
        }
    }
}
